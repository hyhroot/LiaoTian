package src.com.sxt.lt01;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

public class ChatServer {
    List<ServerThread> serthread = new ArrayList<ServerThread>();
    //聊天记录
    List<String> recod = new ArrayList<String>();

    private static int onLine;//在线人数

    public static void main(String[] args) {
        new ChatServer().sta();
    }

    public static int getOnLine() {
        return onLine;
    }

    public static void setOnLine(int onLine) {
        ChatServer.onLine = onLine;
    }

    //服务器启动器
    public void sta() {
        Socket client;
        try {
            ServerSocket server = new ServerSocket(9999);
            int count = 0;
            while (true) {
                count++;
                client = server.accept();
                //每启动连接一个客户端，onLine加1
                setOnLine(getOnLine()+1);
                System.out.println(getOnLine());

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("一个学员上线");

                //广播信息
                for (int i = 0; i < serthread.size(); i++) {
                    //反馈在线人数
//                    serthread.get(i).send(getOnLine());
                    serthread.get(i).send("                               >>>系统提示<<<  ", getOnLine());
                    serthread.get(i).send("                         >>>" + count + "号学员上线<<<", getOnLine());
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    serthread.get(i).send("学员id：" + count + " > 大家好，我是" + count + "号学员,在尚学堂学习",getOnLine());
                }

                //启动线程
                ServerThread st = new ServerThread(client, count);
                new Thread(st).start();
                serthread.add(st);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class ServerThread implements Runnable {

        private Socket s;
        DataInputStream dis;
        DataOutputStream dos , dosl;


        int ID;

        public ServerThread() {
        }

        public ServerThread(Socket client, int id) {
            try {
                this.s = client;
                dis = new DataInputStream(s.getInputStream());
                dos = new DataOutputStream(s.getOutputStream());
                dosl = new DataOutputStream(s.getOutputStream());
                this.ID = id;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void send(String str, int online) {
            try {
                dos.writeUTF(str);
                dosl.writeInt(online);
            } catch (SocketException se) {
//                serthread.remove(this);
//                System.out.println("转发数据异常");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            String in;
            while1:
            while (true) {
                try {
                    try {
                        in = dis.readUTF();
                        //记录聊天信息
                        recod.add(ID + "号学员->" + in);
                        //输出聊天信息
                        for(int j=0;j<recod.size();j++){
                            System.out.print(recod.get(j)+"  ");
                        }

                        System.out.println(ID + " 号学员-> " + in+"---onlein"+onLine);
                            if (in.equals("**t.u.i.c.h.u.**")){
                                //每退出一个学员，onLine减1
                                setOnLine(getOnLine() - 1);
                            }
                        for (int i = 0; i < serthread.size(); i++) {
                            ServerThread s = serthread.get(i);
                            if (in.equals("**t.u.i.c.h.u.**")) {
                                s.send("                                   >>>系统提示<<<",getOnLine());
                                s.send("                            >>>" + ID + " 号学员退出聊天<<<",getOnLine());

//                                System.out.println("退出111111112222222222222222211111111111");
                                dos.flush();
                                dosl.flush();
                            } else {
                                s.send(+ID + " 号学员-> " + in,getOnLine());
                                dos.flush();
                                dosl.flush();
                            }
                        }
                    } catch (EOFException e) {
                        dis.close();
                        dos.close();
                        dosl.close();
                        break;
                    } catch (SocketException se) {
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
