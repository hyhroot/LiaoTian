package src.com.sxt.lt01;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

public class ChatClient extends Frame {

    TextField tfTex = new TextField("请输入聊天信息");
    TextArea taContent = new TextArea(20, 47);

    static Socket client;
    DataOutputStream dos;
    DataInputStream dis;
    DataInputStream disl;
    private static int onLine;     //在线人数
//    int ID;

    public static void main(String[] args) {
        new ChatClient().launchFrame(onLine);
        System.out.println("ONLINE:"+onLine);
    }

    public void launchFrame(int Line) {
        setLocation(400, 200);
        this.setSize(300, 600);
        setTitle("在线人数："+Line);
        add(tfTex, BorderLayout.SOUTH);
        add(taContent, BorderLayout.NORTH);
        pack();

        //关闭窗口
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    //关闭窗口时给服务器发送一个钥匙
                    dos.writeUTF("**t.u.i.c.h.u.**");
//                    dos.writeInt(ID);
                } catch (IOException e1) {
                    e1.printStackTrace();
                    disconnect();
                    System.exit(0);
                }
                disconnect();
                System.exit(0);
            }
        });

        tfTex.addActionListener(new TEListener());
//        //显示窗口
        setVisible(true);
        //连接服务器
        connect();
//        setTitle("在线人数："+onLine);
        //启动线程
        new Thread(new ClientThread()).start();
    }

    public void connect() {
        try {
            client = new Socket("192.168.143.38", 9999);
            taContent.setText("正在连接服务器...............");
            try {
                Thread.sleep(400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            taContent.setText(taContent.getText() + "连接成功!!\n");
            dos = new DataOutputStream(client.getOutputStream());
            dis = new DataInputStream(client.getInputStream());
            disl = new DataInputStream(client.getInputStream());
//            ID = dis.readInt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //关闭流
    public void disconnect() {
        try {
            dos.close();
            client.close();
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class TEListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            String s = tfTex.getText().trim();
            taContent.setText(taContent.getText() + s);
            tfTex.setText("");
            try {
                //信息不能为空
                if (!"".equals(s)) {
                    dos.writeUTF(s);
                    dos.flush();
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    private class ClientThread implements Runnable {

        @Override
        public void run() {
            while (true) {
                try {
                    String st = dis.readUTF();

                    //获取在线人数
//                    onLine = dis.readInt();
                    onLine = disl.readInt();
                    System.out.println("在线人数"+onLine);
//                    launchFrame(onLine);
                    setTitle("在线人数："+onLine);
                    taContent.setText(taContent.getText() + "\n" + st + "\n");
                    taContent.setCaretPosition(taContent.getText().length());
//                    tfTex.setText("");
                } catch (SocketException s) {
                    try {
                        dis.close();
                        dos.close();
                        disl.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}